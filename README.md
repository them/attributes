# Them Attributes

A small set of functions to do the most common task when working with php attributes.

As the API consists of only four static functions, the [source code](src/Attributes.php) should be documentation enough. :D 
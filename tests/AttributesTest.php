<?php

declare(strict_types=1);

namespace Them\Tests\Attributes;

use PHPUnit\Framework\TestCase;
use ReflectionObject;
use Them\Attributes\Attributes;
use Them\Tests\Attributes\Assets\TestAttribute;

/** @psalm-suppress PropertyNotSetInConstructor */
#[TestAttribute('class-level/1')]
#[TestAttribute('class-level/2')]
final class AttributesTest extends TestCase
{
    #[TestAttribute('class-const-level')]
    public const CLASS_CONST = true;

    #[TestAttribute('property-level')]
    public bool $property = true;

    #[TestAttribute('method-level')]
    public function mockMethod(
        #[TestAttribute('param-level')] string $param,
    ): string {
        return $param;
    }

    public function testFromObject(): void
    {
        TestAttribute::assert(
            ['class-level/1', 'class-level/2'],
            Attributes::fromObject($this, TestAttribute::class),
        );

        TestAttribute::assertOne(
            'class-level/1',
            Attributes::oneFromObject($this, TestAttribute::class),
        );
    }

    public function testFromClass(): void
    {
        TestAttribute::assert(
            ['class-level/1', 'class-level/2'],
            Attributes::fromClass(self::class, TestAttribute::class),
        );

        TestAttribute::assertOne(
            'class-level/1',
            Attributes::oneFromClass(self::class, TestAttribute::class),
        );
    }

    public function testFromFunction(): void
    {
        TestAttribute::assert(
            ['method-level'],
            Attributes::fromFunction(
                $this->mockMethod(...),
                TestAttribute::class,
            ),
        );

        TestAttribute::assertOne(
            'method-level',
            Attributes::oneFromFunction(
                $this->mockMethod(...),
                TestAttribute::class,
            ),
        );
    }

    public function testFromReflectionMethod(): void
    {
        $reflection = (new ReflectionObject($this))->getMethod(
            'mockMethod',
        );

        TestAttribute::assert(
            ['method-level'],
            Attributes::fromReflection(
                $reflection,
                TestAttribute::class,
            ),
        );

        TestAttribute::assertOne(
            'method-level',
            Attributes::oneFromReflection(
                $reflection,
                TestAttribute::class,
            ),
        );
    }

    public function testFromReflectionProperty(): void
    {
        $reflection = (new ReflectionObject($this))->getProperty(
            'property',
        );

        TestAttribute::assert(
            ['property-level'],
            Attributes::fromReflection(
                $reflection,
                TestAttribute::class,
            ),
        );

        TestAttribute::assertOne(
            'property-level',
            Attributes::oneFromReflection(
                $reflection,
                TestAttribute::class,
            ),
        );
    }

    public function testFromReflectionClassConstant(): void
    {
        $reflection = (new ReflectionObject($this))->getReflectionConstant(
            'CLASS_CONST',
        );

        TestAttribute::assert(
            ['class-const-level'],
            Attributes::fromReflection(
                $reflection,
                TestAttribute::class,
            ),
        );

        TestAttribute::assertOne(
            'class-const-level',
            Attributes::oneFromReflection(
                $reflection,
                TestAttribute::class,
            ),
        );
    }

    public function testFromReflectionParam(): void
    {
        $reflection = (new ReflectionObject($this))->getMethod(
            'mockMethod',
        )->getParameters()[0];

        TestAttribute::assert(
            ['param-level'],
            Attributes::fromReflection(
                $reflection,
                TestAttribute::class,
            ),
        );

        TestAttribute::assertOne(
            'param-level',
            Attributes::oneFromReflection(
                $reflection,
                TestAttribute::class,
            ),
        );
    }
}

<?php

declare(strict_types=1);

namespace Them\Tests\Attributes\Assets;

use Attribute;
use PHPUnit\Framework\Assert;

#[Attribute(Attribute::TARGET_ALL | Attribute::IS_REPEATABLE)]
final class TestAttribute
{
    public function __construct(
        public readonly string $value,
    ) {
    }

    /**
     * @param string[] $expected
     * @param self[] $attributes
     *
     * @return void
     */
    public static function assert(array $expected, array $attributes): void
    {
        Assert::assertSame(
            $expected,
            array_column($attributes, 'value'),
        );
    }

    /**
     * @param ?string $expected
     * @param ?self $attribute
     *
     * @return void
     */
    public static function assertOne(?string $expected, ?self $attribute): void
    {
        Assert::assertSame(
            $expected,
            $attribute?->value,
        );
    }
}

<?php

declare(strict_types=1);

namespace Them\Attributes;

use Closure;
use ReflectionAttribute as Attr;
use ReflectionClass as Clss;
use ReflectionClassConstant as ClassConst;
use ReflectionException;
use ReflectionFunction as Func;
use ReflectionMethod as Method;
use ReflectionParameter as Param;
use ReflectionProperty as Property;
use ReflectionObject as Obj;

final class Attributes
{
    /**
     * @template T of object
     *
     * @param Method|Func|Clss|ClassConst|Property|Param $reflection
     * @param class-string<T> $attributeClass
     *
     * @return array<T>
     */
    public static function fromReflection(
        Method|Func|Clss|ClassConst|Property|Param $reflection,
        string $attributeClass,
    ): array {
        return array_map(
            static fn(Attr $reflectionAttribute): object =>
                $reflectionAttribute->newInstance(),
            $reflection->getAttributes($attributeClass),
        );
    }

    /**
     * @template T of object
     *
     * @param Method|Func|Clss|ClassConst|Property|Param $reflection
     * @param class-string<T> $attributeClass
     *
     * @return null|T
     */
    public static function oneFromReflection(
        Method|Func|Clss|ClassConst|Property|Param $reflection,
        string $attributeClass,
    ): null|object {
        $attributes = $reflection->getAttributes($attributeClass);

        return $attributes
            ? $attributes[0]->newInstance()
            : null;
    }

    /**
     * @template T of object
     *
     * @param class-string $class
     * @param class-string<T> $attributeClass
     *
     * @return array<T>
     * @throws ReflectionException
     */
    public static function fromClass(
        string $class,
        string $attributeClass,
    ): array {
        return self::fromReflection(
            new Clss($class),
            $attributeClass,
        );
    }

    /**
     * @template T of object
     *
     * @param class-string $class
     * @param class-string<T> $attributeClass
     *
     * @return ?T
     * @throws ReflectionException
     */
    public static function oneFromClass(
        string $class,
        string $attributeClass,
    ): ?object {
        return self::oneFromReflection(
            new Clss($class),
            $attributeClass,
        );
    }

    /**
     * @template T of object
     *
     * @param object $object
     * @param class-string<T> $attributeClass
     *
     * @return array<T>
     */
    public static function fromObject(
        object $object,
        string $attributeClass,
    ): array {
        return self::fromReflection(
            new Obj($object),
            $attributeClass,
        );
    }

    /**
     * @template T of object
     *
     * @param object $object
     * @param class-string<T> $attributeClass
     *
     * @return ?T
     */
    public static function oneFromObject(
        object $object,
        string $attributeClass,
    ): ?object {
        return self::oneFromReflection(
            new Obj($object),
            $attributeClass,
        );
    }

    /**
     * @template T of object
     *
     * @param callable-string|Closure $function
     * @param class-string<T> $attributeClass
     *
     * @return array<T>
     * @throws ReflectionException
     */
    public static function fromFunction(
        string|Closure $function,
        string $attributeClass,
    ): array {
        return self::fromReflection(
            new Func($function),
            $attributeClass,
        );
    }

    /**
     * @template T of object
     *
     * @param callable-string|Closure $function
     * @param class-string<T> $attributeClass
     *
     * @return ?T
     * @throws ReflectionException
     */
    public static function oneFromFunction(
        string|Closure $function,
        string $attributeClass,
    ): ?object {
        return self::oneFromReflection(
            new Func($function),
            $attributeClass,
        );
    }
}
